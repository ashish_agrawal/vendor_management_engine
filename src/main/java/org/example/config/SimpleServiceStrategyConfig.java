package org.example.config;

public class SimpleServiceStrategyConfig {
    Integer failurePercentage;
    Integer successPercentage;
    Integer failureTimeInSecs;
    Integer successTimeInSecs;

    public SimpleServiceStrategyConfig(Integer failurePercentage,  Integer failureTimeInSecs, Integer successPercentage, Integer successTimeInSecs) {
        this.failurePercentage = failurePercentage;
        this.successPercentage = successPercentage;
        this.failureTimeInSecs = failureTimeInSecs;
        this.successTimeInSecs = successTimeInSecs;
    }

    public Integer getFailurePercentage() {
        return failurePercentage;
    }

    public Integer getSuccessPercentage() {
        return successPercentage;
    }

    public Integer getFailureTimeInSecs() {
        return failureTimeInSecs;
    }

    public Integer getSuccessTimeInSecs() {
        return successTimeInSecs;
    }
}
