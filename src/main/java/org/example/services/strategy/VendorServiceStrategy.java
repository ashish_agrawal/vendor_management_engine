package org.example.services.strategy;

import org.example.entities.Vendor;

public interface VendorServiceStrategy {
    public void addVendor(Vendor vendor);
    public String serveRequest(Integer reqId, Integer timeInSecs);
}
