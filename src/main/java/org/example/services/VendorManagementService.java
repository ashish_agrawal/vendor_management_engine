package org.example.services;

import org.example.services.strategy.VendorServiceStrategy;

public class VendorManagementService {
    VendorServiceStrategy serviceStrategy;

    public VendorManagementService(VendorServiceStrategy serviceStrategy) {
        this.serviceStrategy = serviceStrategy;
    }

    public String serveRequest(Integer reqId, Integer timeInSecs) {
        return serviceStrategy.serveRequest(reqId, timeInSecs);
    }
}
