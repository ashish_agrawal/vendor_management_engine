package org.example.helpers;

import java.util.ArrayDeque;
import java.util.Deque;

public class AllocationCounter {
    Deque<Integer> successfulEncounter;
    Deque<Integer> failedEncounter;
    Integer windowSizeInSecs;

    public AllocationCounter(Integer windowSizeInSecs) {
        this.successfulEncounter = new ArrayDeque<>();
        this.failedEncounter = new ArrayDeque<>();
        this.windowSizeInSecs = windowSizeInSecs;
    }

    public void count(Boolean isRequestSuccessful, Integer timeInSecs) {
        if(isRequestSuccessful) {
            successfulEncounter.add(timeInSecs);
        } else {
            failedEncounter.add(timeInSecs);
        }
        clearOlderWindow(timeInSecs);
    }

    public void clearOlderWindow(Integer timeInSecs) {
        while (!successfulEncounter.isEmpty() && successfulEncounter.getFirst() < timeInSecs - windowSizeInSecs)
            successfulEncounter.removeFirst();

        while (!failedEncounter.isEmpty() && failedEncounter.getFirst() < timeInSecs - windowSizeInSecs)
            failedEncounter.removeFirst();
    }

    public Integer getSuccessPercentage() {
        return Math.floorDiv(successfulEncounter.size()*100, successfulEncounter.size() + failedEncounter.size());
    }

    public Integer getFailurePercentage() {
        return Math.floorDiv(failedEncounter.size()*100, successfulEncounter.size() + failedEncounter.size());
    }

    public void reset() {
        this.successfulEncounter = new ArrayDeque<>();
        this.failedEncounter = new ArrayDeque<>();
    }
}
