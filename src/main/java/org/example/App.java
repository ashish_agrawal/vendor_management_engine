package org.example;

import org.example.config.SimpleServiceStrategyConfig;
import org.example.entities.Vendor;
import org.example.services.VendorManagementService;
import org.example.services.availability.AvailabilityService;
import org.example.services.availability.FileBasedAvailabilityService;
import org.example.services.strategy.SimpleServiceStrategy;

import java.io.*;
import java.net.URL;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        System.out.println( "Hello World!" );

        URL urlVendor1 = App.class.getClassLoader().getResource("vendor1.csv");
        URL urlVendor2 = App.class.getClassLoader().getResource("vendor2.csv");
        URL urlVendor3 = App.class.getClassLoader().getResource("vendor3.csv");
        AvailabilityService service1 = new FileBasedAvailabilityService(new File(urlVendor1.getFile()));
        AvailabilityService service2 = new FileBasedAvailabilityService(new File(urlVendor2.getFile()));
        AvailabilityService service3 = new FileBasedAvailabilityService(new File(urlVendor3.getFile()));

        Vendor vendor1 = new Vendor("vendor1", 80, service1);
        Vendor vendor2 = new Vendor("vendor2", 20, service2);
        Vendor vendor3 = new Vendor("vendor2", 0, service3);

        SimpleServiceStrategyConfig config = new SimpleServiceStrategyConfig(40, 300, 80, 300);

        SimpleServiceStrategy serviceStrategy = new SimpleServiceStrategy(config);
        serviceStrategy.addVendor(vendor1);
        serviceStrategy.addVendor(vendor2);
        serviceStrategy.addVendor(vendor3);
        VendorManagementService service = new VendorManagementService(serviceStrategy);

        URL requestsFile = App.class.getClassLoader().getResource("request-time.csv");
        String line = null;
        BufferedReader br = new BufferedReader(new FileReader(new File(requestsFile.getFile())));

        while ((line = br.readLine()) != null) {
            String[] data = line.split(",");
            System.out.println(service.serveRequest(Integer.parseInt(data[0].trim()), Integer.parseInt(data[1].trim())));
        }
    }
}
