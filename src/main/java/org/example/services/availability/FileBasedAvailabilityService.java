package org.example.services.availability;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

/***
 * Stores the availability of service instance for each minute
 */
public class FileBasedAvailabilityService implements AvailabilityService {
    Set<Integer> availability;

    public FileBasedAvailabilityService(File file) {
        this.availability = new HashSet<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String entry = null;

            while ((entry = reader.readLine()) != null) {
                String[] entries = entry.split(",");
                if(Boolean.parseBoolean(entries[1].trim())) {
                    availability.add(Integer.parseInt(entries[0].trim()));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("[ERROR] : File not found " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean isAvailable(Integer timeInSec) {
        return availability.contains(Math.floorDiv(timeInSec, 60));
    }
}
