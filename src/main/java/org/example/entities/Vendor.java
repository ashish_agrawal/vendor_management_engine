package org.example.entities;

import org.example.services.availability.AvailabilityService;

public class Vendor {
    String name;
    Integer allocation;

    AvailabilityService availabilityService;

    public Vendor(String name, Integer allocation, AvailabilityService availabilityService) {
        this.name = name;
        this.allocation = allocation;
        this.availabilityService = availabilityService;
    }

    public String getName() {
        return name;
    }

    public Integer getAllocation() {
        return allocation;
    }

    public Boolean isAvailable(Integer timeInSecs) {
        return availabilityService.isAvailable(timeInSecs);
    }
}
