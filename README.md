This project can be run in 2 ways:

1. Open the application in Intellij Idea and simply run the main method in app class using right-click>run
2. If you have maven installed then you can run the following command to run the main method of the app
	
	```mvn compile exec:java```

