package org.example.services.strategy;

import org.example.config.SimpleServiceStrategyConfig;
import org.example.entities.Vendor;
import org.example.helpers.VendorAllocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleServiceStrategy implements VendorServiceStrategy{
    Random random;
    SimpleServiceStrategyConfig config;
    List<VendorAllocation> vendorAllocations;
    Integer maxAllocation;

    public SimpleServiceStrategy(SimpleServiceStrategyConfig config) {
        this.config = config;
        random = new Random();
        vendorAllocations = new ArrayList<>();
        maxAllocation = 0;
    }

    @Override
    public void addVendor(Vendor vendor) {
        vendorAllocations.add(new VendorAllocation(vendor, config));
        maxAllocation = maxAllocation + vendor.getAllocation();
    }

    @Override
    public String serveRequest(Integer reqId, Integer timeInSecs) {
        Integer allocation = random.nextInt(maxAllocation);
        Integer sum = 0;
        Integer index = 0;
        while (index < vendorAllocations.size() && vendorAllocations.get(index).getCurrentAllocation() + sum < allocation) {
            sum = sum + vendorAllocations.get(index).getCurrentAllocation();
            index++;
        }

        Integer firstIndex = index;
        StringBuilder vendorsTried = new StringBuilder("|");
        while (index < vendorAllocations.size() && !vendorAllocations.get(index).getVendor().isAvailable(timeInSecs)) {
            vendorsTried.append(vendorAllocations.get(index).getVendor().getName());
            vendorsTried.append("|");

            vendorAllocations.get(index).updateCounter(false, timeInSecs);
            index++;
        }
        vendorsTried.append(vendorAllocations.get(index).getVendor().getName());
        vendorAllocations.get(index).updateCounter(true, timeInSecs);

        Integer diff = 0;
        for (; firstIndex< vendorAllocations.size() && (firstIndex<=index || diff!=0); firstIndex++) {
            diff = vendorAllocations.get(firstIndex).updateThresholds(diff);
        }

        return reqId + ", " + vendorsTried.substring(1);
    }

//    This is just for printing current allocation for manual verification
    public void printVendorAllocations(Integer timeInSecs) {
        for (VendorAllocation va : vendorAllocations) {
            System.out.println(va.getVendor().getName() + ", " + va.getCurrentAllocation() + ", " + va.getVendor().isAvailable(timeInSecs));
        }
    }
}
