package org.example.services.availability;

/***
 * Interface to define contracts for different kinds of availability service
 *
 * For this use case we have a file based availability indicator but in real world we can have a
 * heart beat service which can be extended from this interface
 *
 */
public interface AvailabilityService {
    public Boolean isAvailable(Integer timeInSec);
}
