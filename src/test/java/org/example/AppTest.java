package org.example;

import static org.junit.Assert.assertTrue;

import org.example.config.SimpleServiceStrategyConfig;
import org.example.entities.Vendor;
import org.example.services.VendorManagementService;
import org.example.services.availability.AvailabilityService;
import org.example.services.availability.FileBasedAvailabilityService;
import org.example.services.strategy.SimpleServiceStrategy;
import org.example.services.strategy.VendorServiceStrategy;
import org.junit.Test;

import java.io.File;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void basicTest() {
        URL urlVendor1 = getClass().getClassLoader().getResource("vendor1.csv");
        URL urlVendor2 = getClass().getClassLoader().getResource("vendor2.csv");
        AvailabilityService service1 = new FileBasedAvailabilityService(new File(urlVendor1.getFile()));
        AvailabilityService service2 = new FileBasedAvailabilityService(new File(urlVendor2.getFile()));

        Vendor vendor1 = new Vendor("vendor1", 100, service1);
        Vendor vendor2 = new Vendor("vendor2", 0, service2);

        SimpleServiceStrategyConfig config = new SimpleServiceStrategyConfig(10, 30, 20, 15);

        SimpleServiceStrategy serviceStrategy = new SimpleServiceStrategy(config);
        serviceStrategy.addVendor(vendor1);
        serviceStrategy.addVendor(vendor2);
        VendorManagementService service = new VendorManagementService(serviceStrategy);
        System.out.println(service.serveRequest(0, 5));
        System.out.println(service.serveRequest(1, 50));
        System.out.println(service.serveRequest(2, 53));
        System.out.println(service.serveRequest(3, 56));
        System.out.println(service.serveRequest(4, 59));
        System.out.println(service.serveRequest(5, 70));

        serviceStrategy.printVendorAllocations(70);

        System.out.println(service.serveRequest(6, 75));
        System.out.println(service.serveRequest(7, 78));
        System.out.println(service.serveRequest(8, 81));
        System.out.println(service.serveRequest(9, 84));
        System.out.println(service.serveRequest(10, 87));

        serviceStrategy.printVendorAllocations(87);

        System.out.println(service.serveRequest(11, 126));
        System.out.println(service.serveRequest(12, 127));
        System.out.println(service.serveRequest(13, 128));
        System.out.println(service.serveRequest(14, 129));
        System.out.println(service.serveRequest(15, 130));
        System.out.println(service.serveRequest(16, 130));
        System.out.println(service.serveRequest(17, 130));
        System.out.println(service.serveRequest(18, 130));
        System.out.println(service.serveRequest(19, 131));

        serviceStrategy.printVendorAllocations(131);

        System.out.println(service.serveRequest(20, 180));
        System.out.println(service.serveRequest(21, 186));
        System.out.println(service.serveRequest(22, 187));
        System.out.println(service.serveRequest(23, 188));
        System.out.println(service.serveRequest(24, 189));
        System.out.println(service.serveRequest(25, 190));
        System.out.println(service.serveRequest(26, 190));
        System.out.println(service.serveRequest(27, 190));
        System.out.println(service.serveRequest(28, 190));
        System.out.println(service.serveRequest(29, 191));

        serviceStrategy.printVendorAllocations(191);
    }
}
