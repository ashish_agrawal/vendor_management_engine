package org.example.services;

import junit.framework.TestCase;
import org.example.services.availability.AvailabilityService;
import org.example.services.availability.FileBasedAvailabilityService;
import org.junit.Test;

import java.io.File;
import java.net.URL;

public class FileBasedAvailabilityServiceTest extends TestCase {

    @Test
    public void testIsAvailable() {
        URL resource = getClass().getClassLoader().getResource("vendor1.csv");
        AvailabilityService availabilityService = new FileBasedAvailabilityService(new File(resource.getFile()));
        // availability test
        assertTrue(availabilityService.isAvailable(0));
        assertTrue(availabilityService.isAvailable(40));

        // unavailability test
        assertFalse(availabilityService.isAvailable(120));
        assertFalse(availabilityService.isAvailable(179));

        // boundary test
        assertTrue(availabilityService.isAvailable(180));
    }
}