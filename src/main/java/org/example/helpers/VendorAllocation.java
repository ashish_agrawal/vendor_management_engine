package org.example.helpers;

import org.example.enums.AllocationState;
import org.example.config.SimpleServiceStrategyConfig;
import org.example.entities.Vendor;

public class VendorAllocation {
    Vendor vendor;
    Integer currentAllocation;

    AllocationState state;
    AllocationCounter successAllocationCounter;
    AllocationCounter failedAllocationCounter;
    SimpleServiceStrategyConfig config;


    public VendorAllocation(Vendor vendor, SimpleServiceStrategyConfig config) {
        this.vendor = vendor;
        this.currentAllocation = vendor.getAllocation();
        state = AllocationState.SUCCESS;
        successAllocationCounter = new AllocationCounter(config.getSuccessTimeInSecs());
        failedAllocationCounter = new AllocationCounter(config.getFailureTimeInSecs());
        this.config = config;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public Integer getCurrentAllocation() {
        return currentAllocation;
    }

    public void updateCounter(Boolean isRequestSuccessful, Integer timeInSecs) {
        if (state == AllocationState.SUCCESS) {
            failedAllocationCounter.count(isRequestSuccessful, timeInSecs);
        } else {
            successAllocationCounter.count(isRequestSuccessful, timeInSecs);
        }
    }

    public Integer updateThresholds(Integer carry) {
        if (state == AllocationState.SUCCESS) {
            if (failedAllocationCounter.getFailurePercentage() >= config.getFailurePercentage()) {
                Integer newAllocation = Math.floorDiv(currentAllocation, 10);
                Integer diff = currentAllocation - newAllocation;
                currentAllocation = newAllocation;
                state = AllocationState.FAILURE;
                return diff + carry;
            } else {
                if (carry > 0) {
                    currentAllocation = currentAllocation + carry;
                    carry = 0;
                } else {
                    if(currentAllocation > vendor.getAllocation()) {
                        int min = Math.min(currentAllocation - vendor.getAllocation(), Math.abs(carry));
                        currentAllocation = currentAllocation - min;
                        carry = carry + min;
                    }
                }
                return carry;
            }
        } else {
            if (successAllocationCounter.getSuccessPercentage() >= config.getSuccessPercentage()) {
                Integer diff = currentAllocation - vendor.getAllocation();
                currentAllocation = vendor.getAllocation();
                state = AllocationState.SUCCESS;
                return diff + carry;
            } else {
                return carry;
            }
        }
    }
}
